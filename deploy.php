<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'sync.php';

// Project name
set('application',      'Massmovement');
set('repository',       'git@bitbucket.org:codestackers/massmovement.nl.git');
set('home_path',        '/home/massmove/domains/massmovement.nl');
set('storage_folder',   '/home/massmove/domains/storage');

set('default_stage',    'production');

set('branch', 'master');
set('keep_releases', 20);

set('http_user', 'www-data');
set('git_cache', true);

set('ssh_multiplexing', false);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);

set('composer_options', ' install --ignore-platform-reqs  --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader');

set('sync_theme', 'default');

// Hosts

host('production')
    ->hostname('web01.massmovement.nl')
    ->user('massmove')
    ->set('deploy_path', '{{home_path}}');

// Tasks
task('system:update', function () {
    writeln("✶ Set production environment");
    $result = run('cp  {{release_path}}/.env-prod {{release_path}}/.env');
    writeln("✔︎ Result: \n<comment>$result</comment>");

    writeln("✶ Linking storage folder");
    run('mv {{release_path}}/storage {{release_path}}/storage.org');
    $result = run('ln -s {{storage_folder}} {{release_path}}/storage');
    // run('rm -R {{release_path}}/storage.org');
    writeln("✔︎ Result: \n<comment>$result</comment>");
});

task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'sync:theme',
    'deploy:release',
    'deploy:update_code',
    'system:update',
    'deploy:vendors',
    'october:up',
    'artisan:cache:clear',
    'deploy:unlock',
    'deploy:symlink',
    'artisan:up',
    'cleanup'
]);

task('october:up', function () {
    $output = run('{{bin/php}} {{release_path}}/artisan october:up');
    writeln('<info>' . $output . '</info>');
});

task('build', function () {
    run('artisan:cache:clear');
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'artisan:migrate');
