<?php namespace Codestackers\Utils;

use Event;

use Backend;
use Cms\Classes\Theme;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Codestackers\Utils\Models\Settings;

class Plugin extends PluginBase
{
    // public function __construct()
    // {
    //     dd($this->inlineSVG('assets/images/mm-logo-blue.svg',true));
    // }
    public function pluginDetails()
    {
        return [
            'name'        => 'Codestackers utils',
            'description' => 'Some utils for OctoberCMS.',
            'author'      => 'Codestackers',
            'icon'        => 'icon-cog',
            'homepage'    => 'https://codestackers.io/'
        ];
    }
    public function boot()
    {
        Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            if (Settings::get('use_repeater_auto_fold', false))
                $controller->addJs('/plugins/codestackers/utils/assets/js/repeater-auto-fold.js');
            if (Settings::get('use_resize_pane', false)) {
                $controller->addJs('/plugins/codestackers/utils/assets/js/resize-pane.js');
                $controller->addCss('/plugins/codestackers/utils/assets/css/resize.pane.css');
            }
            $controller->addCss('/plugins/codestackers/utils/assets/css/css_language_button.css');
        });
    }
    
    public function registerComponents()
    {
        return [
        ];
    }

    public function registerSettings()
    {
        return [
            'definitions' => [
                'label'         => 'codestackers.utils::lang.plugin.name',
                'description'   => 'codestackers.utils::lang.plugin.description',
                'icon'          => 'icon-cog',
                'category'      => SettingsManager::CATEGORY_CMS,
                'permissions'   => ['codestackers.utils.access_definitions'],
                'class'         => 'Codestackers\Utils\Models\Settings',
                'order'         => 801,
            ]
        ];

    }

    public function registerPageSnippets()
    {
        return $this->registerComponents();
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'inline' => [$this, 'inlineImage'],
                'colorglyph' => [$this, 'colorGlyph'],
                'suffix' => [$this, 'suffix']
            ],
            'functions' => [
                // A static method call, i.e Form::open()
//                'form_open' => ['October\Rain\Html\Form', 'open'],
                'config_get' => ['October\Rain\Support\Facades\Config', 'get']   
            ]
        ];
    }

    private function getSessionFunction()
    {
        return [
            'session' => function ($key = null) {
                return session($key);
            },
        ];
    }
    public  function suffix($input, $min=1, $max=10)
    {
        $suffix = rand($min, $max);
        $ret = $input.$suffix;
        return $ret;
    }
    public function colorGlyph($input=null, $class='header--point', $replace=['.'])
    {
        $ret = $input;
        if($class===null) $class='header--point';
        foreach($replace as $item)
        {
            $span   = '<span class="'.$class.'">'.$item.'</span>';
            $ret    = str_replace($item, $span, $ret);
        }
        return $ret;
    }

    public function inlineImage($file=null, $theme=true, $base64=false )
    {
        $image = "";
        if ($theme)
        {
            $path  = themes_path().'/'.Theme::getActiveTheme()->getDirName().'/'.$file;
        } else {
            $path  = storage_path().'/app/media/'.$file;
        }
        if (file_exists($path))
            $image =  file_get_contents($path);
        if ($base64) $image = base64_encode($image);
        return $image ;
    }
}
