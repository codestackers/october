function updateCountdown(input, comment) {
    var counterEl = comment.find('span');
    var limit = input.data('limit');

    var remaining = limit - input.val().length;
    counterEl.text(remaining);

    counterEl.toggleClass('warning', remaining < 0);
}

function makeCountable(el) {
    var input = el.find(':input');
    var limit = input.data('limit');

    var comment = el.find('.help-block');

    comment.html(comment.html().replace(':limit', limit));

    updateCountdown(input, comment);
}

$(document).on('keyup change', '.countable', function () {
    makeCountable($(this));
});

$(document).on('ajaxSuccess ready', function () {
    $('.countable').each(function () {
        makeCountable($(this));
    });
})