<?php

namespace Renatio\SeoManager\Behaviors;

use Illuminate\Support\Facades\Event;
use October\Rain\Database\Model;
use Renatio\SeoManager\Classes\SeoFields;
use Renatio\SeoManager\Models\SeoTag;
use System\Classes\ModelBehavior;

/**
 * Class SeoModel
 * @package Renatio\SeoManager\Behaviors
 */
class SeoModel extends ModelBehavior
{

    /**
     * @var
     */
    protected $model;

    /**
     * @param $model
     */
    public function __construct($model)
    {
        parent::__construct($model);

        $this->model = $model;

        $this->addRelation();

        $this->extendFields();
    }

    /**
     * @return void
     */
    protected function addRelation()
    {
        $this->model->morphOne['seo_tag'] = [
            SeoTag::class,
            'name' => 'seo_tag',
            'delete' => true,
        ];

        /*
         * Currently RainLab.Translate Plugin does not support relation fields, waiting for fix.
         */
//        $this->model->translatable = array_merge($this->model->translatable, (new SeoTag)->translatable);
    }

    /**
     * @return void
     */
    protected function extendFields()
    {
        Event::listen('backend.form.extendFieldsBefore', function ($widget) {
            if ($this->isModelExtendedWithSeo(optional($widget)->model)) {
                if ($widget->isNested) {
                    return;
                }

                /*
                 * For pivot models isNested property on widget does not work
                 */
                if (array_key_exists('pivot', $widget->model->getRelations())) {
                    return;
                }

                if ( ! $widget->model->seo_tag) {
                    $widget->model->seo_tag = new SeoTag;
                }

                $this->addAssets($widget->getController());

                if ($this->tab() == 'primary') {
                    $widget->tabs['fields'] = $this->mergeFields($widget->tabs['fields']);
                } elseif ($this->tab() == 'secondary') {
                    $widget->secondaryTabs['fields'] = $this->mergeFields($widget->secondaryTabs['fields']);
                }
            }
        });
    }

    /**
     * @return string
     */
    protected function tab()
    {
        return $this->model->methodExists('getSeoTab') ? $this->model->getSeoTab() : 'primary';
    }

    /**
     * @param $controller
     */
    protected function addAssets($controller)
    {
        $controller->addCss('/plugins/renatio/seomanager/assets/css/style.css');
        $controller->addJs('/plugins/renatio/seomanager/assets/js/main.js');
    }

    /**
     * @param $fields
     * @return array
     */
    protected function mergeFields($fields)
    {
        $seoFields = (new SeoFields)->fields();

        if (is_array($fields) && ! empty($fields)) {
            return array_merge($fields, $seoFields);
        }

        return $seoFields;
    }

    /**
     * @param $model
     * @return bool
     */
    protected function isModelExtendedWithSeo($model)
    {
        return $model instanceof Model
            && $model->isClassExtendedWith(static::class);
    }

}