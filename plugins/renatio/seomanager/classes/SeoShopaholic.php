<?php

namespace Renatio\SeoManager\Classes;

use Lovata\Shopaholic\Models\Product;
use Renatio\SeoManager\Behaviors\SeoModel;
use Renatio\SeoManager\Models\SeoTag;
use System\Classes\PluginManager;

/**
 * Class SeoBlog
 * @package Renatio\SeoManager\Classes
 */
class SeoShopaholic
{

    /**
     * @return void
     */
    public function extend()
    {
        if (PluginManager::instance()->exists('Lovata.Shopaholic')) {
            $this->extendModels();
            $this->saveDefaultValues();
        }
    }


    /**
     * @return void
     */
    protected function extendModels()
    {
        $this->extendProductModel();
    }

    /**
     * @return void
     */
    protected function extendProductModel()
    {
        Product::extend(function ($model) {
            $model->implement[] = SeoModel::class;
            $model->addDynamicMethod('getSeoTab', function () {
                return 'primary';
            });
        });
    }

    /**
     * @return void
     */
    protected function saveDefaultValues()
    {
        $this->saveDefaultValuesForProduct();
    }

    /**
     * @return void
     */
    protected function saveDefaultValuesForProduct()
    {
        Product::extend(function ($model) {
            $model->bindEvent('model.afterSave', function () use ($model) {
                $seoTag = $model->seo_tag ?? new SeoTag;

                if (empty($model->seo_tag->meta_title)) {
                    $seoTag->meta_title = str_limit($model->name, 255, '');
                }

                if (empty($model->seo_tag->meta_description)) {
                    $seoTag->meta_description = ! empty($model->preview_text)
                        ? str_limit($model->preview_text, 255, '')
                        : str_limit($model->preview_text, 255, '');
                }

                $model->seo_tag()->save($seoTag);
            });
        });
    }


    /**
     * @return void
     */
    protected function importProduct()
    {
        $this->importRecords(Product::class);
    }


    /**
     * @param $class
     * @param string $titleFrom
     * @param string $descFrom
     */
    protected function importRecords($class, $titleFrom = 'title', $descFrom = 'excerpt')
    {
        $data = [];

        foreach ($class::all() as $record) {
            $data[] = [
                'seo_tag_id' => $record->id,
                'seo_tag_type' => get_class($record),
                'meta_title' => str_limit($record->$titleFrom, 255, null),
                'meta_description' => str_limit($record->$descFrom, 255, null),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        SeoTag::insert($data);
    }

}