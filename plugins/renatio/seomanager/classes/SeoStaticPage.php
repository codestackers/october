<?php

namespace Renatio\SeoManager\Classes;

use RainLab\Pages\Classes\Page;

/**
 * Class SeoStaticPage
 * @package Renatio\SeoManager\Classes
 */
class SeoStaticPage extends SeoCmsPage
{

    /**
     * @var string
     */
    protected $type = Page::class;

    /**
     * @var string
     */
    protected $key = 'viewBag';

    /**
     * @return void
     */
    public static function import()
    {
        foreach (Page::all() as $page) {
            $viewBag = $page->viewBag;

            $viewBag['meta_title'] = $viewBag['meta_title'] ?? $page->title;
            $viewBag['meta_description'] = $viewBag['meta_description'] ?? $page->title;
            $viewBag['robot_index'] = $viewBag['robot_index'] ?? 'index';
            $viewBag['robot_follow'] = $viewBag['robot_follow'] ?? 'follow';

            $data['settings'] = ['viewBag' => $viewBag];

            $page->fill($data);
            $page->forceSave();
        }
    }

}