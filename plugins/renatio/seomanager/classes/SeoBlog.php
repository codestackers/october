<?php

namespace Renatio\SeoManager\Classes;

use RainLab\Blog\Models\Category;
use RainLab\Blog\Models\Post;
use Renatio\SeoManager\Behaviors\SeoModel;
use Renatio\SeoManager\Models\SeoTag;
use System\Classes\PluginManager;

/**
 * Class SeoBlog
 * @package Renatio\SeoManager\Classes
 */
class SeoBlog
{

    /**
     * @return void
     */
    public function extend()
    {
        if (PluginManager::instance()->exists('RainLab.Blog')) {
            $this->extendModels();

            $this->saveDefaultValues();
        }
    }

    /**
     * @return void
     */
    public function import()
    {
        $this->importPosts();

        $this->importCategories();
    }

    /**
     * @return void
     */
    protected function extendModels()
    {
        $this->extendPostModel();

        $this->extendCategoryModel();
    }

    /**
     * @return void
     */
    protected function extendPostModel()
    {
        Post::extend(function ($model) {
            $model->implement[] = SeoModel::class;

            $model->addDynamicMethod('getSeoTab', function () {
                return 'secondary';
            });
        });
    }

    /**
     * @return void
     */
    protected function extendCategoryModel()
    {
        Category::extend(function ($model) {
            $model->implement[] = SeoModel::class;
        });
    }

    /**
     * @return void
     */
    protected function saveDefaultValues()
    {
        $this->saveDefaultValuesForPost();

        $this->saveDefaultValuesForCategory();
    }

    /**
     * @return void
     */
    protected function saveDefaultValuesForPost()
    {
        Post::extend(function ($model) {
            $model->bindEvent('model.afterSave', function () use ($model) {
                $seoTag = $model->seo_tag ?? new SeoTag;

                if (empty($model->seo_tag->meta_title)) {
                    $seoTag->meta_title = str_limit($model->title, 255, '');
                }

                if (empty($model->seo_tag->meta_description)) {
                    $seoTag->meta_description = ! empty($model->excerpt)
                        ? str_limit($model->excerpt, 255, '')
                        : str_limit($model->content, 255, '');
                }

                $model->seo_tag()->save($seoTag);
            });
        });
    }

    /**
     * @return void
     */
    protected function saveDefaultValuesForCategory()
    {
        Category::extend(function ($model) {
            $model->bindEvent('model.afterSave', function () use ($model) {
                $seoTag = $model->seo_tag ?? new SeoTag;

                if (empty($model->seo_tag->meta_title)) {
                    $seoTag->meta_title = str_limit($model->name, 255, '');
                }

                if (empty($model->seo_tag->meta_description)) {
                    $seoTag->meta_description = str_limit($model->name, 255, '');
                }

                $model->seo_tag()->save($seoTag);
            });
        });
    }

    /**
     * @return void
     */
    protected function importPosts()
    {
        $this->importRecords(Post::class);
    }

    /**
     * @return void
     */
    protected function importCategories()
    {
        $this->importRecords(Category::class, 'name', 'name');
    }

    /**
     * @param $class
     * @param string $titleFrom
     * @param string $descFrom
     */
    protected function importRecords($class, $titleFrom = 'title', $descFrom = 'excerpt')
    {
        $data = [];

        foreach ($class::all() as $record) {
            $data[] = [
                'seo_tag_id' => $record->id,
                'seo_tag_type' => get_class($record),
                'meta_title' => str_limit($record->$titleFrom, 255, null),
                'meta_description' => str_limit($record->$descFrom, 255, null),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        SeoTag::insert($data);
    }

}