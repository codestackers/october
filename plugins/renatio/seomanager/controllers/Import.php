<?php

namespace Renatio\SeoManager\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;
use October\Rain\Support\Facades\Flash;
use Renatio\SeoManager\Classes\SeoBlog;
use Renatio\SeoManager\Classes\SeoCmsPage;
use Renatio\SeoManager\Classes\SeoStaticPage;
use System\Classes\SettingsManager;

/**
 * Class Import
 * @package Renatio\SeoManager\Controllers
 */
class Import extends Controller
{

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('October.System', 'system', 'settings');
        SettingsManager::setContext('Renatio.SeoManager', 'import');
    }

    /**
     * @return void
     */
    public function index()
    {
        $this->pageTitle = 'renatio.seomanager::lang.import.label';
    }

    /**
     * @return void
     */
    public function onImportSeoFromCmsPages()
    {
        SeoCmsPage::import();

        Flash::success(trans('renatio.seomanager::lang.import.success'));
    }

    /**
     * @return void
     */
    public function onImportSeoFromStaticPages()
    {
        SeoStaticPage::import();

        Flash::success(trans('renatio.seomanager::lang.import.success'));
    }

    /**
     * @return void
     */
    public function onImportSeoFromBlog()
    {
        (new SeoBlog)->import();

        Flash::success(trans('renatio.seomanager::lang.import.success'));
    }

}