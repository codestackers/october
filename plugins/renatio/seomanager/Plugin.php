<?php

namespace Renatio\SeoManager;

use Backend\Facades\Backend;
use Renatio\SeoManager\Classes\SeoBlog;
use Renatio\SeoManager\Classes\SeoCmsPage;
use Renatio\SeoManager\Classes\SeoStaticPage;
use Renatio\SeoManager\Classes\SeoShopaholic;
use Renatio\SeoManager\Components\SeoTags;
use Renatio\SeoManager\Models\Settings;
use System\Classes\PluginBase;

/**
 * Class Plugin
 * @package Renatio\SeoManager
 */
class Plugin extends PluginBase
{

    /**
     * @var bool
     */
    public $elevated = true;

    /**
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'renatio.seomanager::lang.plugin.name',
            'description' => 'renatio.seomanager::lang.plugin.description',
            'author' => 'Renatio',
            'icon' => 'icon-search',
        ];
    }

    /**
     * @return void
     */
    public function register()
    {
        (new SeoCmsPage)->extend();
        (new SeoStaticPage)->extend();
        (new SeoBlog)->extend();
        (new SeoShopaholic)->extend();
    }

    /**
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'renatio.seomanager.access_settings' => [
                'label' => 'renatio.seomanager::lang.permissions.settings',
                'tab' => 'renatio.seomanager::lang.permissions.tab',
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'renatio.seomanager::lang.settings.label',
                'description' => 'renatio.seomanager::lang.settings.description',
                'category' => 'renatio.seomanager::lang.settings.category',
                'icon' => 'icon-code',
                'class' => Settings::class,
                'order' => 500,
                'keywords' => 'seo sem meta tags',
                'permissions' => ['renatio.seomanager.access_settings'],
            ],
            'import' => [
                'label' => 'renatio.seomanager::lang.import.label',
                'description' => 'renatio.seomanager::lang.import.description',
                'category' => 'renatio.seomanager::lang.settings.category',
                'icon' => 'icon-cloud-download',
                'url' => Backend::url('renatio/seomanager/import'),
                'order' => 510,
                'keywords' => 'seo sem meta tags import',
                'permissions' => ['renatio.seomanager.access_import'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerComponents()
    {
        return [
            SeoTags::class => 'seoTags',
        ];
    }

}