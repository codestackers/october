<?php namespace Indikator\Content\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Backend;
use Indikator\Content\Models\Blog;
use Indikator\Content\Models\News;

class Statistics extends Controller
{
    public $requiredPermissions = ['indikator.content.statistics'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Indikator.Content', 'content', 'statistics');
    }

    public function index()
    {
        $this->prepareGraphs();

        $this->pageTitle = 'indikator.content::lang.menu.statistics';

        $this->addCss('/plugins/indikator/content/assets/css/statistics.css');
    }

    protected function prepareGraphs()
    {
        // Variables

        $this->vars['blog_thisYear'] = $this->vars['blog_lastYear'] = $this->vars['news_thisYear'] = $this->vars['news_lastYear'] = array_fill(0, 13, 0);
        $this->vars['now'] = date('Y');

        /*
         * Blog
         */

        // Graphs

        $this->vars['blog_stat'] = Blog::count();
        $this->vars['blog_view'] = Blog::sum('stat_view');
        $allPosts = Blog::get();

        foreach ($allPosts as $item) {
            $year = substr($item->published_at, 0, 4);
            if ($year == $this->vars['now']) {
                $this->vars['blog_thisYear'][(int)substr($item->published_at, 5, 2)]++;
            }
            else if ($year == $this->vars['now'] - 1) {
                $this->vars['blog_lastYear'][(int)substr($item->published_at, 5, 2)]++;
                $this->vars['blog_lastYear'][0]++;
            }
        }

        // TOP 20

        $amount = Blog::count();
        if ($amount > 20) {
            $amount = 20;
        }

        $items = Blog::orderBy('stat_view', 'desc')->take($amount)->get();
        $this->vars['blog_top'] = '';
        $index = 1;

        foreach ($items as $item) {
            $this->vars['blog_top'] .= '
                <div class="col-md-1 col-sm-1">
                    '.$index.'.
                </div>
                <div class="col-md-9 col-sm-9">
                    <a href="'.Backend::url('indikator/content/blog/update/'.$item->id).'">'.$item->title.'</a>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    '.number_format($item->stat_view, 0, '.', ' ').'
                </div>
                <div class="clearfix"></div>
            ';

            if ($index % 5 == 0) {
                $blog_top .= '<br>';
            }

            $index++;
        }

        // Posts length

        $posts = [];
        foreach ($allPosts as $item) {
            $posts[$item->id] = strlen(trim(preg_replace('/\s+/', ' ', strip_tags($item->summary.$item->content))));
        }

        // Longest posts

        arsort($posts);
        $this->vars['blog_longest'] = '';
        $index = 1;

        foreach ($posts as $id => $length) {
            $item = Blog::whereId($id)->first();

            $this->vars['blog_longest'] .= '
                <div class="col-md-1 col-sm-1">
                    '.$index.'.
                </div>
                <div class="col-md-9 col-sm-9">
                    <a href="'.Backend::url('indikator/content/blog/update/'.$item->id).'">'.$item->title.'</a>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    '.number_format($length, 0, '.', ' ').'
                </div>
                <div class="clearfix"></div>
            ';

            if ($index == 10) {
                break;
            }

            $index++;
        }

        // Shortest posts

        asort($posts);
        $this->vars['blog_shortest'] = '';
        $index = 1;

        foreach ($posts as $id => $length) {
            $item = Blog::whereId($id)->first();

            $this->vars['blog_shortest'] .= '
                <div class="col-md-1 col-sm-1">
                    '.$index.'.
                </div>
                <div class="col-md-9 col-sm-9">
                    <a href="'.Backend::url('indikator/content/blog/update/'.$item->id).'">'.$item->title.'</a>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    '.number_format($length, 0, '.', ' ').'
                </div>
                <div class="clearfix"></div>
            ';

            if ($index == 10) {
                break;
            }

            $index++;
        }

        /*
         * News
         */

        // Graphs

        $this->vars['news_stat'] = News::count();
        $this->vars['news_view'] = News::sum('stat_view');
        $allNews = News::get();

        foreach ($allNews as $item) {
            $year = substr($item->published_at, 0, 4);
            if ($year == $this->vars['now']) {
                $this->vars['news_thisYear'][(int)substr($item->published_at, 5, 2)]++;
            }
            else if ($year == $this->vars['now'] - 1) {
                $this->vars['news_lastYear'][(int)substr($item->published_at, 5, 2)]++;
                $this->vars['news_lastYear'][0]++;
            }
        }

        // TOP 20

        $amount = News::count();
        if ($amount > 20) {
            $amount = 20;
        }

        $items = News::orderBy('stat_view', 'desc')->take($amount)->get();
        $this->vars['news_top'] = '';
        $index = 1;

        foreach ($items as $item) {
            $this->vars['news_top'] .= '
                <div class="col-md-1 col-sm-1">
                    '.$index.'.
                </div>
                <div class="col-md-9 col-sm-9">
                    <a href="'.Backend::url('indikator/content/news/update/'.$item->id).'">'.$item->title.'</a>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    '.number_format($item->stat_view, 0, '.', ' ').'
                </div>
                <div class="clearfix"></div>
            ';

            if ($index % 5 == 0) {
                $this->vars['news_top'] .= '<br>';
            }

            $index++;
        }

        // Posts length

        $posts = [];
        foreach ($allNews as $item) {
            $posts[$item->id] = strlen(trim(preg_replace('/\s+/', ' ', strip_tags($item->summary.$item->content))));
        }

        // Longest posts

        arsort($posts);
        $this->vars['news_longest'] = '';
        $index = 1;

        foreach ($posts as $id => $length) {
            $item = News::whereId($id)->first();

            $this->vars['news_longest'] .= '
                <div class="col-md-1 col-sm-1">
                    '.$index.'.
                </div>
                <div class="col-md-9 col-sm-9">
                    <a href="'.Backend::url('indikator/content/news/update/'.$item->id).'">'.$item->title.'</a>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    '.number_format($length, 0, '.', ' ').'
                </div>
                <div class="clearfix"></div>
            ';

            if ($index == 10) {
                break;
            }

            $index++;
        }

        // Shortest posts

        asort($posts);
        $this->vars['news_shortest'] = '';
        $index = 1;

        foreach ($posts as $id => $length) {
            $item = News::whereId($id)->first();

            $this->vars['news_shortest'] .= '
                <div class="col-md-1 col-sm-1">
                    '.$index.'.
                </div>
                <div class="col-md-9 col-sm-9">
                    <a href="'.Backend::url('indikator/content/news/update/'.$item->id).'">'.$item->title.'</a>
                </div>
                <div class="col-md-2 col-sm-2 text-right">
                    '.number_format($length, 0, '.', ' ').'
                </div>
                <div class="clearfix"></div>
            ';

            if ($index == 10) {
                break;
            }

            $index++;
        }
    }
}
