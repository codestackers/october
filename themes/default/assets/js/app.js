/* 
	APP.JS 
	By Codestackers.io 
*/

var DEBUG = false;
//new WOW().init();

$(document).ready(function () {
	console.log('started');
});

/* October form validation */
$(window).on('ajaxInvalidField', function (event, fieldElement, fieldName, errorMsg, isFirst) {
	event.preventDefault();
	$(fieldElement).addClass('is-invalid');
});

$(document).on('ajaxPromise', '[data-request]', function () {
	$(this).closest('form').find('.is-invalid').removeClass('is-invalid');
});

