const gulp = require('gulp');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');
const npmDist = require('gulp-npm-dist');
var rename = require('gulp-rename');

gulp.task('sass', function() {
    
    return gulp.src('src/scss/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest('dist/css'))
});

gulp.task('watch', function() 
{
    gulp.watch('src/scss/*.scss', gulp.series('sass'));
    gulp.watch('src/scss/**/*.scss', gulp.series('sass'));
    gulp.watch('src/scss/**/**/*.scss', gulp.series('sass'));
});

gulp.task("copyJs", function () {
    gulp.src(npmDist({
                excludes: ['npm'],
                copyUnminified: true,
        }), {
            base:'node_modules/'            
        })
        .pipe(rename(function(path) {
            path.dirname = path.dirname.replace(/\/dist/, '').replace(/\\dist/, '');
        }))
            .pipe(gulp.dest('dist'));
    return 'Copy done';    
});

gulp.task('default', gulp.series('sass'), function() {
    return 'default done';
});
