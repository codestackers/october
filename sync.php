<?php
/* (c) Viacheslav Ostrovskiy <chelout@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Deployer;

/* OPTIONS */

set('sync_theme', 'default');
set('sync_remote_dir', 'public_html/themes');
set('sync_backup', true);

/* TASKS */

desc('Deployment info');
task(
    'sync:info',
    static function () {
        writeln("✔︎ Theme: <comment>" . get('sync_theme') . "</comment>");

        $result = run('pwd');
        writeln("✔︎ Remote current path: <comment>$result</comment>");
        writeln("✔︎ Remote release path: <comment>" . get('release_path') . "</comment>");

        $result = runLocally('pwd');
        writeln("✔︎ Local current path: <comment>" . $result . "</comment>");
        writeln("✔︎ Local deployer path: <comment>" . __DIR__ . "</comment>");
        writeln("✔︎ <comment>DONE</comment>");
        debug();
    }
);

desc('Sync storage to local machine');
task('sync:storage', static function () {
    $stage       = input()->getArgument('stage');
    if (!$stage)
        $stage      = get('default_stage');

    writeln("✔︎ Current stage: <comment>$stage</comment>");

    $host           = host($stage);
    $user           = $host->getUser();
    $hostname       = $host->getRealHostname();
    $deploy_path    = $host->get('deploy_path');

    writeln("✔︎ Sync pages to local for user <comment>$user</comment> path:<comment>$deploy_path</comment>");
    writeln("✔︎ Hostname: <comment>$hostname</comment>");

    rsync_do("$user@$hostname:$deploy_path/public_html/storage/",  __DIR__ . "/storage", "Sync storage");
})->local();


desc('Sync October theme: standard default theme');
task(
    'sync:theme',
    static function () {
        $stage       = input()->getArgument('stage');
        if (!$stage)
            $stage      = get('default_stage');

        writeln("✔︎ Current stage: <comment>$stage</comment>");

        $host           = host($stage);
        $user           = $host->getUser();
        $hostname       = $host->getRealHostname();
        $deploy_path    = $host->get('deploy_path');
        $theme          = get('sync_theme');
        $theme_backup   = get('sync_theme') . '-backup';
        $remote_path    = "$user@$hostname:$deploy_path/" . get('sync_remote_dir');
        $local_path     = __DIR__ . '/themes/';

        writeln("✔︎ Sync pages to local for user <comment>$user</comment> path:<comment>$deploy_path</comment>");
        writeln("✔︎ Hostname: <comment>$hostname</comment>");
        writeln("✔︎ Full path: <comment>$remote_path</comment>");
        writeln("✔︎ Theme: <comment>$theme</comment>");

        if (get("sync_backup")) {
            $result = run("mkdir -p $local_path$theme_backup/$stage/");
            rsync_do("$remote_path/$theme/", "$local_path$theme_backup/$stage", "Backup to $theme_backup/$stage");
        }


        $remote_path    = "$user@$hostname:$deploy_path/" . get('sync_remote_dir');
        $sync_path      = __DIR__ . '/themes/' . $theme;

        rsync_do("$remote_path/$theme/content", $sync_path, 'Content');
        rsync_do("$remote_path/$theme/pages", $sync_path, 'Pages');
        rsync_do("$remote_path/$theme/meta", $sync_path, 'Meta');

        //  rsync -chavzP --stats lovechock@lovechock.codestackers.nl:/home/lovechock/domains/lovechock.com/public_html/themes/default ./themes
        writeln("✔︎ Synced pages to local for user " . $user . " : \n$deploy_path\n<comment>Done</comment>");
    }
)->local();


/* LOCAL SYNC FUNCTIONS */

function rsync_do($from, $to, $comment)
{
    $result = run("rsync -chavzP --stats $from " . $to);
    writeln("✔︎ Sync theme <comment>$comment</comment>");
    rsync_result($result);
}

function rsync_result($result)
{
    //    writeln(OutputInterface::VERBOSITY_DEBUG);
    if (output()->getVerbosity() > 32) {
        writeln("<info>$result</info>");
    }
}
